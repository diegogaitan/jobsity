<?php

return [
        'pagination'    => env('ENTRIES_PAGINATION', 3),
        'count_tweets'  => env('COUNT_TWEETS', 20),
        'number_tweets' => env('NUMBER_TWEETS', 10),
];