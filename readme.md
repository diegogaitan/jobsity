# Diego Gaitán Challenge

------------

## Laravel 5.8 Requirements

* PHP >= 7.1.3
* BCMath PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

## How to Install

1. Install [Composer](https://getcomposer.org/download/ "Composer")
2. composer install

------------

#### Modify .env file

**Database configuration**
```dotenv
DB_DATABASE=dbname
DB_USERNAME=db_user
DB_PASSWORD=
```

**Twitter API keys**
```dotenv
TWITTER_CONSUMER_KEY=
TWITTER_CONSUMER_SECRET=
TWITTER_ACCESS_TOKEN=
TWITTER_ACCESS_TOKEN_SECRET=
```

------------

## Database

#### Via Console

Use the file that is located in */PROJECT_NAME/database/sql/database.sql*
> mysql -u db_user -p <  /PROJECT_NAME/database/sql/database.sql

#### Using Laravel Migrations

Create the database "***dbname***" 
> mysql -u db_user -p > create database dbname

Run these commands in the console
```shell
php artisan migrate
php artisan db:seed
```

------------
Once you enter the application and go to the path */login* you can use this credentials
```html 
email: "user@example.com"
password: "password"
```

------------

There will be several users in the table ***users***  that you can use as well. The password for these users are ***"password"***

------------

## Twitter Privacy

In order to show the tweets in the sidebar, your twitter account **MUST** be public

## Twitter API

- Create an APP in the [Twitter](https://developer.twitter.com/en/apps "Twitter")
- Add this URL in the "*Callback URL*" field in your Twitter Application 
```
http://you_url/profile/twitterCallback
```

------------

## Extras

#### Fresh Database

Run this commands in the console
```shell
php artisan migrate:fresh
php artisan db:seed
```

#### Pagination
If you want to changes the number of item for the pagination modify the variable *ENTRIES_PAGINATION* in the ***".env"*** file. However, by default the number of items per pagination will be ***3***
```
ENTRIES_PAGINATION=
```

#### Number of Tweets

By default the numbers of tweets that the application will retrieve are 20. If you want to change modify the variable *COUNT_TWEETS* in the ***".env"*** file.
```
COUNT_TWEETS=
```

#### Number of Tweets Sidebar

By default the numbers of tweets that are shown in the sidebar are 10. If you want to change modify the variable *NUMBER_TWEETS* in the ***".env"*** file.
```
NUMBER_TWEETS=
```