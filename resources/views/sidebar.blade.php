@isset( $tweets )
    <nav id="sidebar">
        <div id="dismiss">
            <i class="fas fa-arrow-left"></i>
        </div>

        <div class="sidebar-header"></div>

        <ul class="list- components">
            @foreach($tweets as $tweet)
                <li>
                    {!! $tweet->embed !!}
                    @if(auth()->id() == $tweet->user_id)
                        <div class="custom-control custom-switch">
                            <input data-id="{{$tweet->id}}"
                                   type="checkbox" class="custom-control-input toggle-hidden" data-onstyle="success"
                                   data-offstyle="danger" data-toggle="toggle" data-on="Hidden"
                                   data-off="UnHidden"
                                   {{ !$tweet->hidden ? 'checked' : '' }} id="hiddenSwitch_{{ $tweet->id }}">
                            <label class="custom-control-label" for="hiddenSwitch_{{ $tweet->id }}">Show Tweet</label>
                        </div>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>

    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endisset