<div class="col-md-3 ">
    <form action="{{ route('entry_action.destroy' , ['id' => $entry->id]) }}" method="POST">
        @method('DELETE')
        @csrf
        <input type="submit" onclick="return confirm('Are you sure that you want to delete this entry?')" class="btn btn-link no-padding" value="{{ __("Delete Entry") }}">

    </form>
</div>