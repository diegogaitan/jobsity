<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Tweet extends Model
{

    protected $fillable = [
            'tweet', 'tweet_id', 'embedTweet', 'user_id', 'tweet_created_at',
            'tweet_lookup', 'url',
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('notHidden', function (Builder $builder) {
            $builder->where('hidden', false);
        });

        static::addGlobalScope('orderByTweetDate', function (Builder $builder) {
            $builder->orderBy('tweet_created_at', 'desc');
        });

        static::addGlobalScope('limit', function (Builder $builder) {
            $builder->limit(config('jobsity.number_tweets'));
        });

    }

    public static function getUserTweets($id = null)
    {
        if ($id === null && auth()->check()) {
            return self::withoutGlobalScope('notHidden')->where('user_id', Auth::user()->id)->get();
        } elseif ($id === null) {
            return redirect('home');
        }
        return self::where('user_id', $id)->get();
    }

    public function getEmbedAttribute()
    : string
    {
        $tweet_lookup = json_decode($this->tweet_lookup);
        return "<blockquote class='twitter-tweet'>
            <p lang='en' dir='ltr'>{$tweet_lookup->text}</p>
            {$this->user_text} {$this->tweet_url}
            </blockquote>";

    }

    public function getTweetUrlAttribute()
    : string
    {
        $date = date("F j, Y", strtotime($this->tweet_created_at));
        return "<a href='{$this->url}?ref_src=twsrc%5Etfw'>{$date}</a>";
    }

    public function getUserTextAttribute()
    {
        $tweet_lookup = json_decode($this->tweet_lookup);
        return "&mdash; {$tweet_lookup->user->name} ({$tweet_lookup->user->screen_name})";
    }

    public function getIsHiddenAttribute()
    {
        return ( boolean ) $this->getAttribute('hidden');
    }

}
