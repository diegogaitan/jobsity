<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{

    protected $fillable = ['title', 'content'];

    public static function getEntriesPagination($id = null)
    {
        $entries = is_null($id) ? Entry::orderBy('created_at', 'desc')
                : Entry::where("user_id", $id)->orderBy('created_at', 'desc');
        return $entries->paginate(config('jobsity.pagination'));
    }

    public static function printAction(Entry $entry)
    {
        if ($entry->user_id === auth()->user()->id) {
            echo self::find($entry->id)->action_links;
        }
    }

    /**
     * Get the owner to the entry
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDateAndUserAttribute()
    {
        return $this->created_at." by ".$this->user->url;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->rawFormat('M j, Y g:i:s A');
    }

    public function getContentAttribute($value)
    {
        return preg_replace("/\n\n/", "<br />", $value);
    }

    public function getContentPlainAttribute()
    {
        return str_replace('<br />', "\n", $this->content);
    }

    public function getDeleteLinkAttribute()
    {
        return view('entry.formDelete', ['entry' => $this]);
    }

    public function getEditLinkAttribute()
    {
        return '<div class="col-md-3"><a href="'.route("entry_action.edit",
                        ["id" => $this->id]).'">Edit Entry</a></div>';
    }

    public function getActionLinksAttribute()
    {
        return '<div class="row">'.$this->deleteLink.' '.$this->editLink.'</div>';
    }

}
