<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Tweet;
use Illuminate\Http\Request;

class EntryController extends Controller
{

    public function index()
    {
        return view('entries', [
                'entries' => Entry::getEntriesPagination(),
                'tweets'  => auth()->check() ? Tweet::withoutGlobalScope('notHidden')->get() : Tweet::all(),
        ]);
    }

    public function entriesByUser($id)
    {
        return view('entries', [
                'entries' => Entry::getEntriesPagination($id),
                'tweets'  => Tweet::getUserTweets($id),
        ]);
    }
}
