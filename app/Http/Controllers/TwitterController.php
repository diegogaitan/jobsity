<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TwitterApiController as Twitter;
use App\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Thujohn\Twitter\Facades\Twitter as TwitterVendor;

class TwitterController extends Controller
{

    /**
     * @var array
     */
    private $tweetsIds;

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function retrieveTweets()
    {
        $sign_in_twitter = true;
        $force_login     = false;

        // Make sure we make this request w/o tokens, overwrite the default values in case of login.
        Twitter::reconfig(['token' => '', 'secret' => '']);
        $token = Twitter::getRequestToken(route('callbackTwitter'));

        if (isset($token['oauth_token_secret'])) {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

            Session::put('oauth_state', 'start');
            Session::put('oauth_request_token', $token['oauth_token']);
            Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

            return Redirect::to($url);
        }

        return redirect();
    }

    public function callBack()
    {
        if (Session::has('oauth_request_token')) {
            $request_token = [
                    'token'  => Session::get('oauth_request_token'),
                    'secret' => Session::get('oauth_request_token_secret'),
            ];

            Twitter::reconfig($request_token);

            if (Input::has('oauth_verifier')) {
                $oauth_verifier = Input::get('oauth_verifier');
                // getAccessToken() will reset the token for you
                $token = Twitter::getAccessToken($oauth_verifier);
            }

            if (!isset($token['oauth_token_secret'])) {
                return Redirect::route('twitter.error')->with('flash_error', 'We could not log you in on Twitter.');
            }

            $credentials = Twitter::getCredentials();

            if (is_object($credentials) && !isset($credentials->error)) {
                Session::put('access_token', $token);
                Session::put('twitter_user', $credentials);

                $this->saveTweets();
            }
            Session::forget(['access_token', 'twitter_user']);

            return redirect(route('home'));
        }
    }

    public function saveTweets()
    {
        $user   = Auth::user();
        $tweets = Twitter::getUserTimeline();

        foreach ($tweets as $tweet) {
            if (isset($tweet->id)) {
                $lookup = TwitterVendor::getStatusesLookup(['id' => $tweet->id]);
                $lookup = empty($lookup) ? [] : $lookup[0];
                $data   = [
                        'tweet'            => json_encode($tweet),
                        'user_id'          => $user->id,
                        'tweet_lookup'     => json_encode($lookup),
                        'url'              => TwitterApiController::linkTweet($tweet->id),
                        'tweet_created_at' => date('Y-m-d H:i:s', strtotime($tweet->created_at)),
                ];
                Tweet::firstOrCreate(['tweet_id' => $tweet->id], $data);
                $this->tweetsIds[] = $tweet->id;
            }

        }
    }

    public function changeHidden(Request $request)
    {

        $tweet         = Tweet::withoutGlobalScope('notHidden')->find($request->id);
        $tweet->hidden = !$request->hidden;
        $tweet->save();

        return response()->json(['success' => 'The visibility of the tweet was changed successfully.']);
    }

}
