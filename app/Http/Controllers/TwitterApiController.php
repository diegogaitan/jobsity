<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Thujohn\Twitter\Facades\Twitter;

class TwitterApiController extends Twitter
{

    public static function getUserTimeline($object = ['format' => 'object'])
    {
        $collection = collect($object);
        $collection = $collection->merge([
                'screen_name'     => Session::get('twitter_user')->screen_name,
                'count'           => config('jobsity.count_tweets'),
                'exclude_replies' => 1,
        ]);

        return Twitter::getUserTimeline($collection->toArray());
    }

    public static function linkTweet($id)
    {
        return Twitter::linkTweet(Twitter::getTweet($id));
    }

}
