<?php

/** @var Factory $factory */

use App\Entry;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Entry::class, function (Faker $faker) {
    return [
            'title'   => $faker->sentence(),
            'content' => $faker->paragraphs( rand(2,4) , true )
            // 'content' => implode('<br />', $faker->paragraphs(rand(1, 4)))// $faker->randomHtml(2,3)
    ];
});
